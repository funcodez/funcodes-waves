// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.waves;

import java.util.function.BiFunction;

public enum Mixer {

	NONE(( y1, y2 ) -> y2),

	ADD(( y1, y2 ) -> y1 + y2),

	SUBSTRACT(( y1, y2 ) -> y1 - y2),

	MULTIPLY(( y1, y2 ) -> y1 * y2),

	DIVIDE(( y1, y2 ) -> y1 / y2),

	AVERAGE(( y1, y2 ) -> ( y1 + y2 ) / 2);

	private BiFunction<Double, Double, Double> _operation;

	private Mixer( BiFunction<Double, Double, Double> aFunction ) {
		_operation = aFunction;
	}

	public BiFunction<Double, Double, Double> getOperation() {
		return _operation;
	}
}
