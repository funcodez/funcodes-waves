// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.waves;

import static org.refcodes.cli.CliSugar.*;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.Arrays;

import org.refcodes.archetype.CliHelper;
import org.refcodes.audio.BitsPerAmplitude;
import org.refcodes.audio.CsvMonoAmplitudeReader;
import org.refcodes.audio.CsvMonoAmplitudeWriter;
import org.refcodes.audio.CurveFunctionFunction;
import org.refcodes.audio.CurveFunctionSoundAmplitudeBuilder;
import org.refcodes.audio.LineOutMonoAmplitudeWriter;
import org.refcodes.audio.MonoAmplitude;
import org.refcodes.audio.MonoAmplitudeBuilder;
import org.refcodes.audio.MonoAmplitudeWriter;
import org.refcodes.audio.SvgMonoAmplitudeWriter;
import org.refcodes.audio.WavMonoAmplitudeWriter;
import org.refcodes.cli.CliSugar;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.Option;
import org.refcodes.cli.SuperfluousArgsException;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.exception.BugException;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.runtime.Execution;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * Demo application on how to use command line argument parsing with ease using
 * {@link CliSugar} and on how to set up a simple RESTful service with ease
 * using {@link HttpRestServerSugar}.
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "waves";
	private static final String TITLE = "~" + NAME.toUpperCase() + "~";
	private static final String DESCRIPTION = "Tool to generate or pipe and filter (sound) waves for audio playback and export (see [https://www.metacodes.pro/manpages/waves_manpage]).";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/waves_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.SANS_SERIF, FontStyle.PLAIN );

	private enum Format {
		CSV, WAV, SVG, LINE_OUT
	}

	private static final String CURVE_FUNCTION = "curveFunction";
	private static final String MIXER = "mixer";
	private static final String FREQUENCY_HZ = "frequencyHz";
	private static final String AMPLITUDE = "amplitude";
	private static final String SAMPLING_RATE = "samplingRate";
	private static final String LENGTH_SEC = "lengthSec";
	private static final String OUTPUT_FILE = "outputFile";
	private static final String X_OFFSET = "xOffset";
	private static final String Y_OFFSET = "yOffset";
	private static final String FORMAT = "format";
	private static final String BITS_PER_AMPLITUDE = "bitsPerAmplitude";
	private static final BitsPerAmplitude DEFAULT_BITS_PER_SAMPLE = BitsPerAmplitude.HIGH_RES;
	private static final CurveFunctionFunction DEFAULT_CURVE_FUNCTION = CurveFunctionFunction.SINE;
	private static final Mixer DEFAULT_ARITHMETIC_OPERATION = Mixer.NONE;
	private static final Format DEFAULT_FILE_FORMAT = Format.CSV;
	private static final int DEFAULT_SAMPLING_RATE_PER_SEC = 44100;
	private static final double DEFAULT_AMPLITUDE = 1D;
	private static final double DEFAULT_LENGTH_SEC = 1D;
	private static final int DEFAULT_X_OFFSET = 0;
	private static final double DEFAULT_Y_OFFSET = 0;
	private static final String COMMENT = "Generated by <FUNCODES.CLUB> ~WAVES~ (http://www.funcodes.club)";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {
		final Option<Double> theFrequencyOpt = doubleOption( 'f', "frequency", FREQUENCY_HZ, "The frequency (Hz) to use when generating the values." );
		final Option<CurveFunctionFunction> theCurveFunctionOpt = enumOption( 'c', "curve-function", CurveFunctionFunction.class, CURVE_FUNCTION, "The function to use when generating the values (defaults to " + DEFAULT_CURVE_FUNCTION.name() + "). The function can be one of the following: " + VerboseTextBuilder.asString( CurveFunctionFunction.values() ) );
		final Option<Mixer> theMixerOpt = enumOption( 'm', "mixer", Mixer.class, MIXER, "The mixer operation to be applied to the data from standard input. The function can be one of the following: " + VerboseTextBuilder.asString( Mixer.values() ) );
		final Option<Double> theAmplitudeOpt = doubleOption( 'a', "amplitude", AMPLITUDE, "The (max) amplitude (0..n) to use when generating the values (defaults to " + DEFAULT_AMPLITUDE + ")." );
		final Option<Integer> theSamplingRateOpt = intOption( 's', "sampling-rate", SAMPLING_RATE, "The sample rate (per second) for the generated values (defaults to " + DEFAULT_SAMPLING_RATE_PER_SEC + " samples/second)." );
		final Option<Double> theLengthOpt = doubleOption( 'l', "length", LENGTH_SEC, "The length (in seconds) for the generated values (defaults to " + DEFAULT_LENGTH_SEC + " seconds)." );
		final Option<Double> theYOffsetOpt = doubleOption( 'y', "y-offset", Y_OFFSET, "The y-offset (floating-point ) on the y-axis for the amplitude to use when generating the values (defaults to " + DEFAULT_Y_OFFSET + ")." );
		final Option<Integer> theXOffsetOpt = intOption( 'x', "x-offset", X_OFFSET, "The x-offset (integer) on the x-axis for the samples offset to use when generating the values (defaults to " + DEFAULT_X_OFFSET + ")." );
		final Option<File> theOutputFileOpt = fileOption( 'o', "output-file", OUTPUT_FILE, "The output file (file name) where to write the values to." );
		final Option<Format> theFormatOpt = enumOption( 'F', "format", Format.class, FORMAT, "The output Format to use when writing to an output file (defaults to " + DEFAULT_FILE_FORMAT.name() + "). The Format can be one of the following: " + VerboseTextBuilder.asString( Format.values() ) );
		final Option<BitsPerAmplitude> theBitsPerAmplitudeOpt = enumOption( 'b', "bits", BitsPerAmplitude.class, BITS_PER_AMPLITUDE, "The bits/amplitude to use if supported by the output Format (defaults to " + DEFAULT_BITS_PER_SAMPLE.name() + "). The bits/amplitude: " + VerboseTextBuilder.asString( BitsPerAmplitude.values() ) );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theDebugFlag = debugFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();

		// @formatter:off
		final Term theArgsSyntax = cases ( 
			and( theFrequencyOpt, theCurveFunctionOpt, 
				optional( theAmplitudeOpt, theXOffsetOpt, theYOffsetOpt, theVerboseFlag, theDebugFlag,
					xor(
						optional( theLengthOpt, theSamplingRateOpt, optional( theOutputFileOpt, theFormatOpt, theBitsPerAmplitudeOpt ) ),
						and( theMixerOpt, optional( theOutputFileOpt, theFormatOpt, theBitsPerAmplitudeOpt ) )
					)
				)
			),
			xor( theHelpFlag, theSysInfoFlag )
		);
		final Example[] theExamples = examples(
			example( "Set <" + FORMAT + "> to '" + Format.LINE_OUT + "' for direct playback", theCurveFunctionOpt, theLengthOpt, theFrequencyOpt, theFormatOpt ),
			example( "Generate waves for further processing", theCurveFunctionOpt, theFrequencyOpt, theLengthOpt ),
			example( "Pipe ('|') previous output in here ", theCurveFunctionOpt, theFrequencyOpt, theMixerOpt, theFormatOpt )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		try {
			if ( theFormatOpt.getValue() == Format.LINE_OUT && theOutputFileOpt.hasValue() ) {
				throw new SuperfluousArgsException( "The format <" + theFormatOpt.getValue() + "> (as of " + Arrays.toString( theCliHelper.toOptions( theFormatOpt ) ) + ") cannot be used together with an output file (as of " + Arrays.toString( theCliHelper.toOptions( theOutputFileOpt ) ) + ").", theCliHelper.toOptions( theOutputFileOpt ) );
			}
			final CurveFunctionFunction theTrigonometricFunction = theCurveFunctionOpt.getValueOr( DEFAULT_CURVE_FUNCTION );
			final double theFrequency = theFrequencyOpt.getValue();
			final double theAmplitude = theAmplitudeOpt.getValueOr( DEFAULT_AMPLITUDE );
			final int theSamplingRate = theSamplingRateOpt.getValueOr( DEFAULT_SAMPLING_RATE_PER_SEC );
			final double theLength = theLengthOpt.getValueOr( DEFAULT_LENGTH_SEC );
			final int theXOffset = theXOffsetOpt.getValueOr( DEFAULT_X_OFFSET );
			final double theYOffset = theYOffsetOpt.getValueOr( DEFAULT_Y_OFFSET );
			final Mixer theArithmeticOperation = theMixerOpt.getValueOr( DEFAULT_ARITHMETIC_OPERATION );
			final Format theFormat = theFormatOpt.getValueOr( DEFAULT_FILE_FORMAT );
			final BitsPerAmplitude theBitsPerAmplitude = theBitsPerAmplitudeOpt.getValueOr( DEFAULT_BITS_PER_SAMPLE );
			final File theOutputFile = theOutputFileOpt.getValue();
			// @formatter:off
			try ( MonoAmplitudeWriter<?> theMonoWriter = switch ( theFormat ) {
				case LINE_OUT -> new LineOutMonoAmplitudeWriter(theBitsPerAmplitude );
				case CSV -> new CsvMonoAmplitudeWriter( toOutStream( theOutputFile ) );
				case SVG -> new SvgMonoAmplitudeWriter( toOutStream( theOutputFile ) );
				case WAV -> ( theOutputFile != null ) ? new WavMonoAmplitudeWriter( theOutputFile, theBitsPerAmplitude ) : new WavMonoAmplitudeWriter( toOutStream(), theBitsPerAmplitude );
				default -> {
					throw new BugException( "Encountered an unconsidered value <" + theFormat + "> for type <" + Format.class.getName() + ">" );
				}
			} ) 
			// @formatter:on
			{
				if ( theMixerOpt.hasValue() ) {
					filterWavesTo( theTrigonometricFunction, theArithmeticOperation, theFrequency, theAmplitude, theXOffset, theYOffset, theMonoWriter, theVerboseFlag.isEnabled() );
				}
				else {
					generateWavesTo( theTrigonometricFunction, theFrequency, theAmplitude, theXOffset, theYOffset, theSamplingRate, theLength, theMonoWriter, theVerboseFlag.isEnabled() );
				}
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// GENERATE:
	// /////////////////////////////////////////////////////////////////////////

	private static void generateWavesTo( CurveFunctionFunction aTrigonemetricFunction, double aFrequencyHz, double aAmplitude, int aXOffset, double aYOffset, int aSamplingRate, double aLengthSec, MonoAmplitudeWriter<?> aMonoWriter, boolean isVerbose ) throws IOException {

		if ( isVerbose ) {
			System.out.println( "# " + COMMENT );
			System.out.println( "# Trigonometric function = " + aTrigonemetricFunction );
			System.out.println( "# Frequency (Hz) = " + aFrequencyHz );
			System.out.println( "# Amplitude (max) = " + aAmplitude );
			System.out.println( "# Length (seconds) = " + aLengthSec );
			System.out.println( "# X-Offset (samples) = " + aXOffset );
			System.out.println( "# Y-Offset = " + aYOffset );
			System.out.println( "# Sampling rate (per second) = " + aSamplingRate );
			System.out.println();
		}

		int theSamples = (int) ( aLengthSec * aSamplingRate ) + 1;
		MonoAmplitudeBuilder eSample;
		try ( aMonoWriter ) {
			for ( int i = 0; i < theSamples; i++ ) {
				eSample = CurveFunctionSoundAmplitudeBuilder.asMonoSample( i, aTrigonemetricFunction.getFunction(), aFrequencyHz, aAmplitude, aXOffset, aYOffset, aSamplingRate );
				aMonoWriter.writeNext( eSample.getMonoData() );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// FILTER:
	// /////////////////////////////////////////////////////////////////////////

	private static void filterWavesTo( CurveFunctionFunction aTrigonemetricFunction, Mixer aMixer, double aFrequencyHz, double aAmplitude, int aXOffset, double aYOffset, MonoAmplitudeWriter<?> aMonoWriter, boolean isVerbose ) throws IOException, ParseException {

		if ( isVerbose ) {
			System.out.println( "# " + COMMENT );
			System.out.println( "# Trigonometric function = " + aTrigonemetricFunction );
			System.out.println( "# Frequency (Hz) = " + aFrequencyHz );
			System.out.println( "# Amplitude (max) = " + aAmplitude );
			System.out.println( "# X-Offset (samples) = " + aXOffset );
			System.out.println( "# Y-Offset = " + aYOffset );
			System.out.println();
		}

		try ( aMonoWriter; CsvMonoAmplitudeReader theCsvReader = new CsvMonoAmplitudeReader( new BufferedInputStream( System.in ) ) ) {
			double eSampleData;
			MonoAmplitude eFromSample;
			MonoAmplitudeBuilder eToSample;
			while ( theCsvReader.hasNext() ) {
				eFromSample = theCsvReader.nextRow();
				eToSample = CurveFunctionSoundAmplitudeBuilder.asMonoSample( eFromSample.getIndex(), aTrigonemetricFunction.getFunction(), aFrequencyHz, aAmplitude, aXOffset, aYOffset, eFromSample.getSamplingRate() );
				eSampleData = aMixer.getOperation().apply( eFromSample.getMonoData(), eToSample.getMonoData() );
				eToSample.setMonoData( eSampleData );
				aMonoWriter.writeNext( eToSample );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// SAMPLE:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static PrintStream toOutStream( File aOutputFile ) throws FileNotFoundException {
		if ( aOutputFile != null ) {
			return new PrintStream( new FileOutputStream( aOutputFile ) );
		}
		return Execution.toBootstrapStandardOut();
	}

	private static PrintStream toOutStream() {
		return Execution.toBootstrapStandardOut();
	}

	// /////////////////////////////////////////////////////////////////////////
	// DAEMONS:
	// /////////////////////////////////////////////////////////////////////////
}
