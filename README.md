# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

**The [`WAVES`](https://bitbucket.org/funcodez/funcodes-waves/src/master/src/main/java/club/funcodes/waves/Main.java) (`funcodes-waves`) experimentation box is a command line tool for generating and processing [sinusoid](https://en.wikipedia.org/wiki/Sinusoid) audio data, immediately playing the result to your computer's "audio out" line or saving it into [`WAV`](https://en.wikipedia.org/wiki/WAV) or [`CSV`](https://en.wikipedia.org/wiki/Comma-separated_values) files. Harnessing your shell's pipe and filter mechanism you can apply freely succeeding processing steps.**

## Usage ##

See the [`WAVES`](https://www.metacodes.pro/manpages/waves_manpage) manpage for a complete user guide, basic usage instructions can be queried as follows:

```
$ ./waves-launcher-x.y.z.sh --help
```

## Downloads ##

For a variety of readily built executables please refer to the [downloads](https://www.metacodes.pro/downloads) section of the [`METACODES.PRO`](https://www.metacodes.pro) site.

## Getting started ##

To get up and running, clone the [`funcodes-waves`](https://bitbucket.org/funcodez/funcodes-waves/) repository from [`bitbucket`](https://bitbucket.org/funcodez/funcodes-waves)'s `git` repository.

## How do I get set up? ##

Using `SSH`, go as follows to get the [`Maven`](https://en.wikipedia.org/wiki/Apache_Maven) [`CSV`](https://bitbucket.org/funcodez/funcodes-waves/) project:

```
git clone git@bitbucket.org:funcodez/funcodes-waves.git
```

Using `CSV`, go accordingly as follows to get the [`Maven`](https://en.wikipedia.org/wiki/Apache_Maven) [`CSV`](https://bitbucket.org/funcodez/funcodes-waves/) project:

```
git clone https://bitbucket.org/funcodez/funcodes-waves.git
```

Then you can build a [`fat-jar`](https://maven.apache.org/plugins/maven-shade-plugin/examples/executable-jar.html) and launch the application: 

## Big fat executable bash script (optional) ##

This step is optional, though when running your application under `Linux`, the following will be your friend:

> To build a big fat single executable [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) script, take a look at the [`scriptify.sh`](https://bitbucket.org/funcodez/funcodes-waves/src/master/scriptify.sh) script and the [`build.sh`](https://bitbucket.org/funcodez/funcodes-waves/src/master/build.sh) script respectively:

```
./scriptify.sh
./target/waves-launcher-x.y.z.sh
```

The resulting `waves-launcher-x.y.z.sh` file is a big fat single executable [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) script being launched via `./target/waves-launcher-x.y.z.sh`.

> Building and creating an executable bash script is done by calling `./build.sh`!

## First steps ##

Go for `./target/waves-launcher-x.y.z.sh --help` (or `java -jar target/funcodes-waves-0.0.1.jar` if you wish) to get instructions on how to invoke the tool.

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/funcodez/funcodes-waves/issues)
* Add a nifty user-interface
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
